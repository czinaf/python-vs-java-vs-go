
import java.util.Random;
import java.util.ArrayList;
import java.util.List;
import java.io.OutputStreamWriter;
import java.io.File;
import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.BufferedReader;
import java.io.FileReader;

public class Perf {

	private static final int SIZE = 1000000;

	public static void main(String... args) throws Exception {
		Random random = new Random();
		long startTime = System.currentTimeMillis();
		float[] f = new float[SIZE];
		
		for(int i = 0; i < SIZE; i++) {
			f[i] = random.nextFloat();
		}
		
		float[] c = new float[SIZE];
		
		for(int i = 0; i < SIZE; i++) {
			c[i] = f[i] * f[i];
		}
		
		File fout = new File("testfile");
		FileOutputStream fos = new FileOutputStream(fout);
 
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));
 
		for (float number : c) {
			bw.write(String.valueOf(number));
			bw.newLine();
		}
 
		bw.close();
		
		BufferedReader br = new BufferedReader(new FileReader(new File("testfile")));
 
		String line = null;
		List<String> content = new ArrayList<>();
		while ((line = br.readLine()) != null) {
			content.add(line);
		}
 
		br.close();
	
		long endTime = System.currentTimeMillis();
		
		System.out.println("StartTime: " + startTime + " endTime:" + endTime);
		System.out.println("Time: " + (endTime - startTime));
	}

}