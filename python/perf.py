import random
import time

SIZE = 1000000

startTime = int(round(time.time() * 1000))

f = []

for i in range(0, SIZE):
	f.append(random.random())
	
c = []

for i in range(0, SIZE):
	c.append(f[i] * f[i])
	
output = open('testfile','w')

for i in range(0, SIZE):
	output.write("%f\n" % c[i])
output.close()	

with open('testfile') as f:
    content = f.readlines()
	
endTime = int(round(time.time() * 1000))

print("startTime: %d, endTime: %d" % (startTime, endTime))
print("Time: %d" % (endTime - startTime))