package main


import (
	"fmt"
	"math/rand"
	"time"
	"os"
	"bufio"
)

func main() {
	
	startTime := time.Now().UnixNano() / int64(time.Millisecond)
	r := rand.New(rand.NewSource(99))
	var f [1000000]float32
	for i := 0; i < 1000000; i++ {
		f[i] = r.Float32()
	}
	endTime1 := time.Now().UnixNano() / int64(time.Millisecond)
	fmt.Printf("Time 1: %d\n", (endTime1 -startTime))
	
	var c [1000000]float32
	for i := 0; i < 1000000; i++ {
		c[i] = f[i] * f[i]
	}
	endTime2 := time.Now().UnixNano() / int64(time.Millisecond)
	fmt.Printf("Time 2: %d\n", (endTime2 -startTime))
	
	output, err := os.Create("testfile")
	wr := bufio.NewWriter(output)
	
	for i := 0; i < 1000000; i++ {
		fmt.Fprintf(wr, "%f", c[i])
		fmt.Fprintln(wr)
	}
	output.Close()
	endTime3 := time.Now().UnixNano() / int64(time.Millisecond)
	fmt.Printf("Time 3: %d\n", (endTime3 -startTime))
	input, err := os.Open("testfile")
	var lines []string
	scanner := bufio.NewScanner(input)
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}
	input.Close()
	endTime4 := time.Now().UnixNano() / int64(time.Millisecond)
	fmt.Printf("Time 4: %d\n", (endTime4 -startTime))
	endTime := time.Now().UnixNano() / int64(time.Millisecond)
	
	fmt.Printf("startTime: %d endTime: %d\n", startTime, endTime)
	fmt.Printf("Time: %d", (endTime -startTime))
	
	if err != nil {
        panic(err)
    }
}